# SCube Data Preparation:
# This code is used to handle the formatting and segregation
# that should be done on the scube data. 

#Libraries 
library(readxl)
library(tidyverse)

setwd("C:/Users/Kadri Gbemi/Desktop/thesis/segregationdataanalysis/analysis/data/")


## Download Imputed Scube data
scube_data_raw_imp <- read.csv(file="segregation/imputationData/rf_imp_scube_data.csv", header=TRUE, sep=",", fileEncoding = "UTF-8")

#Format scube data county value
scube_data_imp <- scube_data_raw_imp %>% mutate(County = str_replace(County, "maa", ""))

regions <- sapply(scube_data_imp$County, function(county){
  region_name <- switch(county, Harju="Northern Estonia", L��ne="Western Estonia", P�rnu="Western Estonia",
                        Saare="Western Estonia", J�rva="Central Estonia", "L��ne-Viru"="Central Estonia", 
                        Rapla="Central Estonia", "Ida-Viru"="Northeastern Estonia", J�geva="Southern Estonia", 
                        P�lva="Southern Estonia", Tartu="Southern Estonia", Valga="Southern Estonia", 
                        Viljandi="Southern Estonia", V�ru="Southern Estonia")
  region_name
})
scube_data_imp$Regions <- regions

# Change timeUnit from categorical to annual
scube_data_imp$timeUnit <- sub("-1-1","", scube_data_imp$timeUnit)
scube_data_imp$timeUnit <- sub("-2-1","", scube_data_imp$timeUnit)
scube_data_imp$timeUnit <- as.integer(scube_data_imp$timeUnit)


#Convert regions column from list to character
scube_data_imp$Regions <- as.character(scube_data_imp$Regions)

## Calculate the mean of segregation indexes of different companies to easily 
#measure for one county
calculateMean <- function(x){
  x[["Dissimilarity"]] <- mean(x[["Dissimilarity"]])
  x[["Gini"]] <- mean(x[["Gini"]])
  x[["Entropy"]] <- mean(x[["Entropy"]])
  x[["Isolation"]] <- mean(x[["Isolation"]])
  x[["Interaction"]] <- mean(x[["Interaction"]])
  x[["Atkinson"]] <- mean(x[["Atkinson"]])
  x
}

# * Declare a function that segregates the seperated data according to the 
# county where the company resides with respect to the age of the board members within a certain amount of years.
# 
# * In short words this function will split the data by 'age', 'County' and 
# 'timeUnit' Column. 
# 
# * This function is declared in order to easily measure certain values 
# for the segregated data without having to repeat this process as a loop 
# countless times.

segregationMean <- function(segregationSplitData){
  segregatedData <- lapply(segregationSplitData, function(x){
    calculateMean(x) 
  })
  data <- data.frame(Reduce(rbind, segregatedData))
  data
}

### Generate average mean for segregation indexes 

# * Select the combinded data according to 'County', 'Age', 'sex', 'Rate', 
# 'Activities' and 'TimeUnit'. 
# 
# * This function is declared to calculate the average mean of each
# segregated data.

segregationDataSplit <- function(data){
  split(data, list(data[["sex"]], data[["age"]], data[["County"]], data[["timeUnit"]]), drop = TRUE)
}

scube_data <- segregationMean(segregationDataSplit(scube_data_imp)) 

# ## Export Formatted Scube imputed data as segregation data
write_csv(scube_data, "segregation/segregation_data.csv")
