library(readxl)
library(tibble)
library(tidyr)
library(readr)
library(dplyr)
library(plyr)
#Set specific working directory
setwd("C:/Users/Kadri Gbemi/Desktop/thesis/segregationdataanalysis/analysis/data/")

#Download Indicators data

#Jobs Indicator By gender Male and female
employment_data <- read_excel("employment_gap/employment_gap_data.xlsx", skip = 1)
employment_rate_raw_data <- employment_data[1:2,]
employment_gap_raw_data <- employment_data[3,]

#Employment gap data from wide to long format
employment_rate_data <- gather(employment_rate_raw_data, timeUnit, employmentRate, `1997`:`2019`, factor_key=TRUE)
employment_gap_data <- gather(employment_gap_raw_data, timeUnit, employmentGap, `1997`:`2019`, factor_key=TRUE)
employment_gap <- select(employment_gap_data, c("timeUnit", "employmentGap"))

unemployed_by_duration_raw_data <- read.csv(file="labor-market/unemployed_by_duration/unemployed_by_duration_well_being.csv", header=TRUE, sep=",", fileEncoding = "UTF-8")
unemployed_by_duration_raw_data <- unemployed_by_duration_raw_data[unemployed_by_duration_raw_data$sex != 'M and females',]


unemployed_by_region_raw_data <- read.csv(file="labor-market/unemployed_by_region/unemployment_region_well_being.csv", header=TRUE, sep=",", fileEncoding = "UTF-8")
unemployed_by_region_raw_data <- unemployed_by_region_raw_data[unemployed_by_region_raw_data$Regions != 'Whole country' & unemployed_by_region_raw_data$timeUnit >= 2004,]

#Finance Index Data
finance_raw_data <- read.csv(file="finance/finance_data_well_being.csv", header=TRUE, sep=",", fileEncoding = "UTF-8")
finance_raw_data <- finance_raw_data[finance_raw_data$County != 'Whole country' &
                                       finance_raw_data$timeUnit >= 2004,]

dataMerge1 <- Reduce(function(x,y) merge(x = x, y = y, by=c("timeUnit", "sex")),
                     list(employment_rate_data, finance_raw_data, unemployed_by_duration_raw_data))

#Health By Regions Indicator (Because the health Indicator data is from 2004 to 2016 we will only 
# measure well-being for this time period).
health_data <- read_excel("health/health_data.xlsx", skip = 2)

#Wages Cutoff
wagesCutoff <- data.frame(timeUnit=c(2004:2016), 
                          wageCutOff = c(2480/15.6466,2690/15.6466, 3000/15.6466,
                                         3600/15.6466, 4350/15.6466, 4350/15.6466, 
                                         4350/15.6466, 278.02, 290, 320, 355, 390, 430))

dataMerge2 <- Reduce(function(x,y) merge(x = x, y = y, by=c("timeUnit")),
                     list(dataMerge1,  health_data, employment_gap, wagesCutoff))
# Poverty Indicator
poverty_data <- read_excel("poverty/poverty_data.xlsx", skip = 2)

dataMerge3 <- Reduce(function(x,y) merge(x = x, y = y, by=c("timeUnit", "Regions")),
                     list(dataMerge2, poverty_data, unemployed_by_region_raw_data))

# Income or Net Wages Indicator
wages_data <- read_excel("poverty/wages_data.xlsx", skip = 2)
wages_data$County <- sub(" county", "", wages_data$County)
wages_data <- wages_data[wages_data$County != 'Whole country',]

#Download Segregation Index data
scube_data_raw <- read.csv(file="segregation/segregation_data.csv", header=TRUE, sep=",", fileEncoding = "UTF-8")
scube_data_raw <- scube_data_raw[scube_data_raw$County != 'Whole country' &
                                   scube_data_raw$timeUnit >= 2004,]
well_being_measure <- Reduce(function(x,y) merge(x = x, y = y, by=c("County", "timeUnit")),
                     list(dataMerge3, wages_data))

scube_data_filter <- unique(select(scube_data_raw, c("County", "timeUnit", "age", "Dissimilarity",
                                              "Entropy", "Gini", "Isolation", "Interaction", "Atkinson")))

well_being_seg <- merge(scube_data_filter, well_being_measure, by=c("County", "timeUnit", "age"))

handleDeprivationCond <- function(type, deprivationValue, comparison, cutoffValue){
  getDeprivation <- switch(type, "deprivation" = deprivationValue,
                           "normalizedGap" = (cutoffValue-comparison)/cutoffValue, 
                           "squaredGap" = sqrt((cutoffValue-comparison)/cutoffValue))
  return (getDeprivation)
}

handleCutoff<- function(vector, cutoffValue, moreThanValue, lessThanValue, type){
  newVector <- lapply(vector, function(x){
    if(x > cutoffValue){
      if(moreThanValue == 0){
        x <- handleDeprivationCond("deprivation", moreThanValue, x, cutoffValue) 
      }else{
        x <- handleDeprivationCond(type, moreThanValue, x, cutoffValue)
      }
    }else {
      if(lessThanValue == 0){
        x <- handleDeprivationCond("deprivation", lessThanValue, x, cutoffValue)
      }else{
        x <- handleDeprivationCond(type, lessThanValue, x, cutoffValue)
      }
    }
    return (x)
  })
  return (as.numeric(newVector))
}

handleCutoffRange<- function(vector, startCutoffValue, endCutoffValue, moreThanValue, lessThanValue, type){
  newVector <- lapply(vector, function(x){
    if(x >= startCutoffValue & x <= endCutoffValue){
      if(moreThanValue == 0){
        x <- handleDeprivationCond("deprivation", moreThanValue, x, endCutoffValue) 
      }else{
        x <- handleDeprivationCond(type, moreThanValue, x, endCutoffValue)
      }
    }else {
      if(lessThanValue == 0){
        x <- handleDeprivationCond("deprivation", lessThanValue, x, endCutoffValue)
      }else{
        x <- handleDeprivationCond(type, lessThanValue, x, endCutoffValue)
      }
    }
    return (x)
  })
  return (as.numeric(newVector))
}
handleWagesCutoff <-  function(data, type){
  newData <- select(data, c("AverageMonthlyNet", "wageCutOff"))
  wageDeprivation <- c()
  if(type == "normalizedGap"){
    wageDeprivation = (newData$wageCutOff-newData$AverageMonthlyNet)/newData$wageCutOff 
  }
  
  if(type == "squaredGap"){
    wageDeprivation = sqrt(((newData$wageCutOff-newData$AverageMonthlyNet)/newData$wageCutOff)) 
  }
  if(type == "deprivation"){
    newData$deprivation <- newData$AverageMonthlyNet < newData$wageCutOff
    wageDeprivation <- lapply(newData$deprivation, function(x){
      if(x == TRUE){
        x <- 1
      }else{
        x <- 0
      }
      return (x)
    })
  }

  return (as.numeric(wageDeprivation))
}

handleDeprivation <- function(data, type){
    data$employmentGapDeprivation <- handleCutoff(data$employmentGap, 5, 1, 0, type)
    data$employmentRateDeprivation <- handleCutoff(data$employmentRate, 60, 0, 1, type)
    data$unEmployedDurationDeprivation <- handleCutoff(data$unEmployedDurationPerCent, 4.5, 1, 0, type)
    data$health_VeryGoodOrGood_Deprivation <- handleCutoff(data$`(Health) Very good or good`, 90, 0, 1, type)
    data$health_NeitherGoodNorBad_Deprivation <- handleCutoff(data$`(Health) Neither good nor bad`, 7, 1, 0, type)
    data$health_BadOrVeryBad_Deprivation <- handleCutoff(data$`(Health) Bad or very bad`, 3, 1, 0, type)
    data$QuickRatioDeprivation <- handleCutoff(data$QuickRatio, 0.94, 0, 1, type)
    data$BurnRateDeprivation <- handleCutoff(data$BurnRate, 0, 0, 1, type)
    data$RunwayDeprivation <- handleCutoff(data$Runway, -0.5, 0, 1, type)
    data$ROEDeprivation <- handleCutoffRange(data$ReturnOnEquity, 15, 20, 0, 1, type)
    data$CurrentRatioDeprivation <- handleCutoffRange(data$CurrentRatio, 1.2, 2, 0, 1, type)
    data$AbsolutePovertyDeprivation <- handleCutoff(data$`AbsolutePovertyRate%`, 3, 1, 0, type)
    data$WagesDeprivation <- handleWagesCutoff(data, type)
    return(data)
}
# 
# #Calculating Well-being Index function
# deprivationData <- function(data, type){
#   well_being_index_data = NULL
# for(t in unique(data$timeUnit, incomparables = FALSE)){
#   for(r in unique(data$Regions, incomparables = FALSE)){
#     for(c in unique(data$County, incomparables = FALSE)){
#     
#       d <- filter(data, County == c & Regions == r & timeUnit == t)
#       if(nrow(d) > 0){
#        deprivation <- handleDeprivation(d, type)
#       well_being_index_data = rbind.fill(well_being_index_data, deprivation)
#       }
#     }
#   }}
#   return (well_being_index_data)
# }

wellnessDeprivation<- function(well_being_measure_data){
  deprivation_index<- handleDeprivation(well_being_measure_data, "deprivation")
  
  deprivation_index$WellbeingDeprivation <- (deprivation_index$employmentGapDeprivation + deprivation_index$employmentRateDeprivation
                                            + deprivation_index$unEmployedDurationDeprivation + deprivation_index$health_VeryGoodOrGood_Deprivation
                                            + deprivation_index$health_NeitherGoodNorBad_Deprivation + deprivation_index$health_BadOrVeryBad_Deprivation
                                            + deprivation_index$QuickRatioDeprivation + deprivation_index$BurnRateDeprivation + deprivation_index$RunwayDeprivation 
                                            + deprivation_index$ROEDeprivation)/10
  
  # Females well-being data
  wellness_deprivation_data<- select(deprivation_index, c(1:11, 43))

  return (wellness_deprivation_data)
}

well_being_data <- wellnessDeprivation(well_being_seg)
## Export merged Scube data with labor data
write_csv(well_being_data, "well-being/well_being_data.csv")
