Properties of the original data:
Research one: Labour market indicators (
Unemployment rate by year, County and age, 
Inactive persons in workforce by year, County and age,
Unemployment by duration (Total, Less than 6 months, 6 to 11 months, 
12 months or more), sex and year,
Unemployment by Region and year
).

Finance data is by year and county.
Research two: Financial Indicators (
*Current Ratio: (Current Assets at year end) / (Current liabilities at year end),
*Quick Ratio: (Current Assets at year end) – (inventories at year end) –
(prepayments at year end)/ (current liabilities at year end),
*Burn Rate: (Cash spent at the begining of the year) - (Cash spent at the end of the year)/12
*Runway: (Cash spent at the end of the year)/ (Burn Rate)
*Return on Equity: (Net income) / (Average share holder equity)*100
)

Current Ratio:= measures a company's ability to pay short-term obligations or those due within one year

Quick Ratio:= a company's capacity to pay its current liabilities without needing to sell its inventory or get additional financing

Burn rate := is the rate at which a company is losing money.

Runway:= refers to the amount of time a company has before it runs out of cash.

Return on Equity:=  of how well the company utilizes its equity to generate profits.
 In other words, an ROE indicates a company’s ability to turn equity capital into net profit.


Shareholders' equity (or business net worth) shows how much the owners
of a company have invested in the business—either by investing money in it
 or by retaining earnings over time.

Finance data is by year and county.
Set cut-off for each indicator:
Research three: Well-being deprivation data (
Employment gap, employment rate, unemployment by duration,
Health of persons 16 and older, by place of residence and health status,
Quick ratio, burn rate, runway, ROE, current ratio, 
poverty (poverty and material deprivation rate, average monthly wages data)
)

