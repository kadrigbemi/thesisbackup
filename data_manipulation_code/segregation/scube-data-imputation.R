# SCube Data Imputation:
# This code is used to handle missing scube data. 

#Libraries 
library(readxl)
library(tidyverse)
library(missForest)
library(doParallel)

setwd("D:/thesisbackup/results/")

#Download original segregation data (SCube)
scube_data <- read_excel("C:/Users/Kadri Gbemi/Desktop/thesis/segregationdataanalysis/analysis/data/segregation/scube-data.xlsx")

# Calculate percentage of missing data 

#TotalRowsAndColumns = (num of rows = nrow(scube_data))* (num of columns = ncol(scube_data))= 1833930

# (sum of na's in data = sum(is.na(scube_data))/TotalRowsAndColumns)* 100 = 31.14023%

# (571090/1833930)*100 = 31.14023

##Convert columns data type that contain characters to a factor type 
# This conversion will assist to help eliminate character bugs from missForest function
scube_data$residence <- as.factor(scube_data$residence)
scube_data$age <- as.factor(scube_data$age)
scube_data$sex <- as.factor(scube_data$sex)
scube_data$legalType <- as.factor(scube_data$legalType)
scube_data$shareCapital <- as.factor(scube_data$shareCapital)
scube_data$workersHigh <- as.factor(scube_data$workersHigh)
scube_data$RegistrationDistrict <- as.factor(scube_data$RegistrationDistrict)
scube_data$County <- as.factor(scube_data$County)
scube_data$City <- as.factor(scube_data$City)
scube_data$partitionGraphAlgorithm <- as.factor(scube_data$partitionGraphAlgorithm)
scube_data$timeUnit <- as.factor(scube_data$timeUnit)

#Imputation of missing values with Random forest
registerDoParallel(cores=3)
rf_imp_scube_data <- missForest(as.data.frame(scube_data), ntree = 200, verbose = TRUE,
                                maxiter = 10, parallelize= 'forests')

#Check imputed data
scube_data_rf_imp <- rf_imp_scube_data$ximp
scube_data_rf_imp

#Check error estimate of imputed data
scube_data_rf_imp_error <- rf_imp_scube_data$OOBerror
scube_data_rf_imp_error


#Export Data from R & Error Estimate

write.table(scube_data_rf_imp, file="segregation/imputationData/rf_imp_scube_data.csv", sep=",")
write.table(scube_data_rf_imp_error, file="segregation/imputationData/rf_imp_scube_data_OOBerror.csv", sep=",")

