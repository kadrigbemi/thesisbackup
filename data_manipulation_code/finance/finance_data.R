library(readxl)
library(readr)

#Set specific working directory
setwd("D:/thesisbackup/analysis/data/")

#Download all finance data
cash_raw_data <- read_excel("finance/cash_spent/cash_data.xlsx", skip = 1)
current_assets_raw_data <- read_excel("finance/current-assets/current-assets-data.xlsx")
current_liabilities_raw_data <- read_excel("finance/current-liabilities/current_liabilities_data.xlsx")
income_raw_data <- read_excel("finance/income/income_data.xlsx")
inventories_raw_data <- read_excel("finance/inventories/inventories_data.xlsx")
prepayments_raw_data <- read_excel("finance/prepayments/prepayments_data.xlsx")
supplier_payable_raw_data <- read_excel("finance/supplier-payable/supplier_payable_data.xlsx")
total_liabilities_raw_data <- read_excel("finance/total-liabilities/total_liabilities_data.xlsx")
total_assets_raw_data <- read_excel("finance/total-assets/total_assets_data.xlsx", skip = 5)

#Download segregation data
scube_data_raw <- read.csv(file="segregation/segregation_data.csv",
                           header=TRUE, sep=",", fileEncoding = "UTF-8")

translate_county_value <- function(data){
  data$County <- sub(" county", "", data$County)
  return(data)
}

cash_data <- translate_county_value(cash_raw_data)
current_assets_data <- translate_county_value(current_assets_raw_data)
current_liabilities_data <- translate_county_value(current_liabilities_raw_data)
income_data <- translate_county_value(income_raw_data)
inventories_data <- translate_county_value(inventories_raw_data)
prepayments_data <- translate_county_value(prepayments_raw_data)
supplier_payable_data <- translate_county_value(supplier_payable_raw_data)
total_liabilities_data <- translate_county_value(total_liabilities_raw_data)
total_assets_data <- translate_county_value(total_assets_raw_data)

print(nrow(cash_data)+nrow(current_assets_data)+nrow(current_liabilities_data)+
        +nrow(income_data)+nrow(inventories_data)+nrow(prepayments_data)
      +nrow(total_liabilities_data)+nrow(total_assets_data))

finance_data <- Reduce(function(x,y) merge(x = x, y = y, by=c("County", "timeUnit")),
       list(cash_data, current_assets_data, current_liabilities_data, total_liabilities_data,
            income_data, total_assets_data, inventories_data, prepayments_data, supplier_payable_data))
finance_scube_data <- merge(finance_data, scube_data_raw, by=c("County", "timeUnit"))

finance_scube_data$CurrentRatio <- finance_scube_data$CurrentAssetsYearEnd/ finance_scube_data$CurrentLiabilitiesYearEnd 
finance_scube_data$QuickRatio <- (finance_scube_data$CurrentAssetsYearEnd - finance_scube_data$InventoriesYearEnd - finance_scube_data$prepaymentYearEnd)/finance_scube_data$CurrentLiabilitiesYearEnd 
finance_scube_data$BurnRate <- (finance_scube_data$CashYearBegin - finance_scube_data$CashYearEnd)/12 
finance_scube_data$Runway <- finance_scube_data$CashYearEnd / finance_scube_data$BurnRate
finance_scube_data$ShareholderAtBegin <- finance_scube_data$totalAssetsYearBegin- finance_scube_data$LiabilitiesTotalYearBegin
finance_scube_data$ShareholderAtEnd <- finance_scube_data$totalAssetsYearEnd - finance_scube_data$LiabilitiesTotalYearEnd
finance_scube_data$AvgShareHolderEquity <- (finance_scube_data$ShareholderAtBegin + finance_scube_data$ShareholderAtEnd)/2
finance_scube_data$ReturnOnEquity <- (finance_scube_data$FinancialIncome / finance_scube_data$AvgShareHolderEquity)*100

finance_data_main <- select(finance_scube_data, c("County", "timeUnit", "age", "sex", "CurrentRatio", 
                                            "QuickRatio", "BurnRate", "Runway", "ReturnOnEquity"))

## Export merged Scube data with labor data
write_csv(finance_scube_data, "finance/finance_data.csv")

#For well-being data
write_csv(finance_data_main, "finance/finance_data_well_being.csv")
